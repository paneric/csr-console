# CSR - generator

Controller, Service, Repository, Query, Aggregated Data Access Object, Data Access Object etc. classes generator.

## Installation

(1) Install with composer:

```sh
$ composer install paneric/csr-console
``` 

(2) Copy _**app**_ file from: 

```sh
$ my-project/vendor/paneric/csr-console/bin/
``` 

(3) Paste to: 

```sh
$ my-project/bin/
``` 

(4) Set scope value in _**app**_ file as '_**app**_':

```php
#!/usr/bin/env php
<?php

$scope = 'app';
...
``` 

(5) Start creating your classes.

## Data Access Layer
 
### All DAL package classes

```sh
$ bin/app dal --mode=dal

Class name: Article
Module name: Article
psr-4: App
DB table field prefix: art
DAO attributes names: id,ref
DAO attributes types: int,string
```

Remarks:

* **Class**: class name. Must be entered with no suffix.

### ADAO - Aggregated data access object

```sh
$ php bin/app dal --mode=adao

Class name: Article
Module name: Article
psr-4: App
Namespaces of aggregated DAOs: App\DAL\User\,App\DAL\Credential\
Types aggregated DAOs: UserDAO,CredentialDAO
Names of aggregated DAOs: userDAO,credentialDAO
``` 

Remarks:

* **Class**: class name. Must be entered without _**ADAO**_ suffix.

### DAO - Data access object

```sh
$ php bin/app dal --mode=dao

Class name: Article
Module name: Article
psr-4: App
DB table field prefix: art
DAO attributes names: id,ref
DAO attributes types: int,string
``` 

Remarks:

* **Class**: class name. Must be entered without _**DAO**_ suffix.
* **DB table field prefix**: must be entered without **_** suffix.
    
### Repository

```sh
$  php bin/app dal --mode=rep

Class name: Article
Module name: Article
psr-4: App
```

Remarks:

* **Class**: class name. Must be entered without _**Repository**_ suffix.

### Query

```sh
$  php bin/app dal --mode=qry

Class name: Article
Module name: Article
psr-4: App
```

Remarks:

* **Class**: class name. Must be entered without _**Query**_ suffix.

## Gateway Layer
 
### All Gateway Layer package classes

```sh
$ bin/app gtw --mode=gtw

Class name: Article
Module name: Article
psr-4: App
```
Remarks:

* **Class**: class name. Must be entered with no suffix.

### Event

```sh
$ php bin/app gtw --mode=evt

Class name: Article
Module name: Article
psr-4: App
```

Remarks:

* **Class**: class name. Must be entered without _**Event**_ suffix.

### Listener Service

```sh
$ php bin/app gtw --mode=lst

Class name: Article
Module name: Article
psr-4: App
```

Remarks:

* **Class**: class name. Must be entered without _**ListenerService**_ suffix.

## Interfaces
 
### All interfaces

```sh
$ bin/app ifc --mode=ifc

Class name: Article
Module name: Article
psr-4: App
```
Remarks:

* **Class**: class name. Must be entered with no suffix.

### Repository Interface

```sh
$ bin/app ifc --mode=ifcr

Class name: Article
Module name: Article
psr-4: App
```
Remarks:

* **Class**: class name. Must be entered with no _**RepositoryInterface**_ suffix.

### Query Interface

```sh
$ bin/app ifc --mode=ifcq

Class name: Article
Module name: Article
psr-4: App
```
Remarks:

* **Class**: class name. Must be entered with no _**QueryInterface**_ suffix.


## Business Logic Layer

### All BLL package classes

```sh
$ php bin/app bll --mode=bll

Class name: Article
Module name: Article
psr-4: App
```

Remarks:

* **Class**: class name. Must be entered with no suffix.

### Controller

```sh
$ php bin/app bll --mode=ctrl

Class name: Article
Module name: Article
psr-4: App
```

Remarks:

* **Class**: class name. Must be entered without _**Controller**_ suffix.

### Service

```sh
$ php bin/app bll --mode=srv

Class name: Article
Module name: Article
psr-4: App
```

Remarks:

* **Class**: class name. Must be entered without _**Service**_ suffix.

### ShowOneByIdAction

```sh
$ php bin/app bll --mode=acro

Class name: Article
Module name: Article
psr-4: App
```

Remarks:

* **Class**: class name. Must be entered without _**GetOneByIdAction**_ suffix.

### ShowAllPaginatedAction

```sh
$ php bin/app bll --mode=acrap

Class name: Article
Module name: Article
psr-4: App
```

Remarks:

* **Class**: class name. Must be entered without _**GetAllPaginatedAction**_ suffix.

### AddAction

```sh
$ php bin/app bll --mode=acc

Class name: Article
Module name: Article
psr-4: App
```

Remarks:

* **Class**: class name. Must be entered without _**CreateAction**_ suffix.

### EditAction

```sh
$ php bin/app bll --mode=acu

Class name: Article
Module name: Article
psr-4: App
```

Remarks:

* **Class**: class name. Must be entered without _**UpdateAction**_ suffix.

### RemoveAction

```sh
$ php bin/app bll --mode=acd

Class name: Article
Module name: Article
psr-4: App
```

Remarks:

* **Class**: class name. Must be entered without _**DeleteAction**_ suffix.

### DTO

```sh
$ php bin/app bll --mode=dto

Class name: Article
Module name: Article
psr-4: App
```

Remarks:

* **Class**: class name. Must be entered without _**DTO**_ suffix.

### Handler

```sh
$ php bin/app bll --mode=hnd

Class name: Article
Module name: Article
psr-4: App
```

Remarks:

* **Class**: class name. Must be entered without _**Handler**_ suffix.