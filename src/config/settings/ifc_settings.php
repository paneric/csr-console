<?php

return [

    'ifcr' => [
        'template' => APP_FOLDER . 'config/templates/ifc/repository_interface_template.txt',
        'patterns' => ['/{psr}/', '/{Module}/',  '/{Class}/',  '/{class}/', '/{classes}/'],
        'methods'  => [ 'setPsr',  'setModule', 'setUcClass', 'setLcClass',  'setClasses'],
        'values'   => [    'psr',     'module',      'class',      'class',       'class'],
    ],

    'ifcq' => [
        'template' => APP_FOLDER . 'config/templates/ifc/query_interface_template.txt',
        'patterns' => ['/{psr}/', '/{Module}/',  '/{Class}/',  '/{class}/', '/{classes}/'],
        'methods'  => [ 'setPsr',  'setModule', 'setUcClass', 'setLcClass',  'setClasses'],
        'values'   => [    'psr',     'module',      'class',      'class',       'class'],
    ],

];
