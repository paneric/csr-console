<?php

return [

    'dao' => [

        'template' => APP_FOLDER . 'config/templates/dal/dao_template.txt',
        'patterns' => ['/{psr}/', '/{Module}/',  '/{Class}/',  '/{prefix}/'],
        'methods'  => [ 'setPsr',  'setModule', 'setUcClass', 'setLcPrefix'],
        'values'   => [    'psr',     'module',      'class',      'prefix'],

        'statements_patterns' => ['/{attributes}/', '/{getters}/', '/{setters}/'],
        'statements' => [
            'attributes' => [
                'template' => <<<END
    protected \${attribute};//{type}
END,
                'patterns' => [ '/{attribute}/',  '/{type}/'],
                'methods'  => ['setLcAttribute', 'setLcType'],
                'values'   => [     'attribute',      'type'],
            ],
            'getters' => [
                'template' => <<<END
    public function get{Attribute}(): ?{Type}
    {
        return \$this->{attribute};
    }
END,
                'patterns' => [ '/{attribute}/',  '/{Attribute}/',  '/{Type}/'],
                'methods'  => ['setLcAttribute', 'setUcAttribute', 'setUcType'],
                'values'   => [     'attribute',      'attribute',      'type'],
            ],
            'setters' => [
                'template' => <<<END
    public function set{Attribute}({Type} \${attribute}): void
    {
        \$this->{attribute} = \${attribute};
    }
END,
                'patterns' => [ '/{attribute}/',  '/{Attribute}/',  '/{Type}/'],
                'methods'  => ['setLcAttribute', 'setUcAttribute', 'setUcType'],
                'values'   => [     'attribute',      'attribute',      'type'],
            ],
        ],
    ],

    'adao' => [
        'template' => APP_FOLDER . 'config/templates/dal/adao_template.txt',
        'patterns' => ['/{psr}/', '/{Module}/',  '/{Class}/'],
        'methods'  => [ 'setPsr',  'setModule', 'setUcClass'],
        'values'   => [    'psr',     'module',      'class'],

        'statements_patterns' => ['/{uses}/', '/{attributes}/', '/{constructor}/', '/{getters}/', '/{setters}/'],
        'statements' => [
            'uses' => [
                'template' => <<<END
use {Namespace}{Type};
END,
                'patterns' => [ '/{Namespace}/', '/{Type}/'],
                'methods'  => ['setUcNamespace','setUcType'],
                'values'   => [     'namespace',     'type'],
            ],
            'attributes' => [
                'template' => <<<END
    protected \${attribute};//{Type}
END,
                'patterns' => [ '/{attribute}/',  '/{Type}/'],
                'methods'  => ['setLcAttribute', 'setUcType'],
                'values'   => [     'attribute',      'type'],
            ],
            'constructor' => [
                'template' => <<<END
        \$this->{attribute} = new {Type}();
        \$this->values = \$this->{attribute}->hydrate(\$this->values);
        
END,
                'patterns' => [ '/{attribute}/',  '/{Type}/'],
                'methods'  => ['setLcAttribute', 'setUcType'],
                'values'   => [     'attribute',      'type'],
            ],
            'getters' => [
                'template' => <<<END
    public function get{Attribute}(): ?{Type}
    {
        return \$this->{attribute};
    }
END,
                'patterns' => [ '/{attribute}/',  '/{Attribute}/',  '/{Type}/'],
                'methods'  => ['setLcAttribute', 'setUcAttribute', 'setUcType'],
                'values'   => [     'attribute',      'attribute',      'type'],
            ],
            'setters' => [
                'template' => <<<END
    public function set{Attribute}({Type} \${attribute}): void
    {
        \$this->{attribute} = \${attribute};
    }
END,
                'patterns' => [ '/{attribute}/',  '/{Attribute}/',  '/{Type}/'],
                'methods'  => ['setLcAttribute', 'setUcAttribute', 'setUcType'],
                'values'   => [     'attribute',      'attribute',      'type'],
            ],
        ],
    ],

    'rep' => [
        'template' => APP_FOLDER . 'config/templates/dal/repository_template.txt',
        'patterns' => ['/{psr}/', '/{Module}/', '/{Class}/', '/{classes}/'],
        'methods'  => [ 'setPsr',  'setModule', 'setUcClass', 'setClasses'],
        'values'   => [    'psr',     'module',      'class',      'class'],
    ],

    'qry' => [
        'template' => APP_FOLDER . 'config/templates/dal/query_template.txt',
        'patterns' => ['/{psr}/', '/{Module}/', '/{Class}/', '/{classes}/'],
        'methods'  => [ 'setPsr',  'setModule', 'setUcClass', 'setClasses'],
        'values'   => [    'psr',     'module',      'class',      'class'],
    ],
];
