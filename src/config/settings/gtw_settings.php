<?php

return [

    'evt' => [
        'template' => APP_FOLDER . 'config/templates/gtw/event_template.txt',
        'patterns' => ['/{psr}/', '/{Module}/',  '/{Class}/',  '/{class}/', '/{classes}/'],
        'methods'  => [ 'setPsr',  'setModule', 'setUcClass', 'setLcClass',  'setClasses'],
        'values'   => [    'psr',     'module',      'class',      'class',       'class'],
    ],

    'lst' => [
        'template' => APP_FOLDER . 'config/templates/gtw/listener_template.txt',
        'patterns' => ['/{psr}/', '/{Module}/',  '/{Class}/',  '/{class}/', '/{classes}/'],
        'methods'  => [ 'setPsr',  'setModule', 'setUcClass', 'setLcClass',  'setClasses'],
        'values'   => [    'psr',     'module',      'class',      'class',       'class'],
    ],
];
