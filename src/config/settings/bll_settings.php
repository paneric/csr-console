<?php

return [

    'ctrl' => [
        'template' => APP_FOLDER . 'config/templates/bll/controller_template.txt',
        'patterns' => ['/{psr}/', '/{Module}/', '/{module_route}/',  '/{Class}/',  '/{class}/', '/{classes}/'],
        'methods'  => [ 'setPsr',  'setModule',   'setModuleRoute', 'setUcClass', 'setLcClass',  'setClasses'],
        'values'   => [    'psr',     'module',           'module',      'class',      'class',       'class'],
    ],

//    'srv' => [
//        'template' => APP_FOLDER . 'config/templates/bll/service_template.txt',
//        'patterns' => ['/{psr}/', '/{Module}/',  '/{Class}/',  '/{class}/', '/{classes}/'],
//        'methods'  => [ 'setPsr',  'setModule', 'setUcClass', 'setLcClass',  'setClasses'],
//        'values'   => [    'psr',     'module',      'class',      'class',       'class'],
//    ],

    'acc' => [
        'template' => APP_FOLDER . 'config/templates/bll/add_action_template.txt',
        'patterns' => ['/{psr}/', '/{Module}/',  '/{Class}/',  '/{class}/', '/{classes}/'],
        'methods'  => [ 'setPsr',  'setModule', 'setUcClass', 'setLcClass',  'setClasses'],
        'values'   => [    'psr',     'module',      'class',      'class',       'class'],
    ],

    'acrap' => [
        'template' => APP_FOLDER . 'config/templates/bll/show_ap_action_template.txt',
        'patterns' => ['/{psr}/', '/{Module}/',  '/{Class}/',  '/{class}/', '/{classes}/'],
        'methods'  => [ 'setPsr',  'setModule', 'setUcClass', 'setLcClass',  'setClasses'],
        'values'   => [    'psr',     'module',      'class',      'class',       'class'],
    ],

    'acro' => [
        'template' => APP_FOLDER . 'config/templates/bll/show_o_action_template.txt',
        'patterns' => ['/{psr}/', '/{Module}/',  '/{Class}/',  '/{class}/', '/{classes}/'],
        'methods'  => [ 'setPsr',  'setModule', 'setUcClass', 'setLcClass',  'setClasses'],
        'values'   => [    'psr',     'module',      'class',      'class',       'class'],
    ],

    'acu' => [
        'template' => APP_FOLDER . 'config/templates/bll/edit_action_template.txt',
        'patterns' => ['/{psr}/', '/{Module}/',  '/{Class}/',  '/{class}/', '/{classes}/'],
        'methods'  => [ 'setPsr',  'setModule', 'setUcClass', 'setLcClass',  'setClasses'],
        'values'   => [    'psr',     'module',      'class',      'class',       'class'],
    ],

    'acd' => [
        'template' => APP_FOLDER . 'config/templates/bll/remove_action_template.txt',
        'patterns' => ['/{psr}/', '/{Module}/',  '/{Class}/',  '/{class}/', '/{classes}/'],
        'methods'  => [ 'setPsr',  'setModule', 'setUcClass', 'setLcClass',  'setClasses'],
        'values'   => [    'psr',     'module',      'class',      'class',       'class'],
    ],

    'dto' => [
        'template' => APP_FOLDER . 'config/templates/bll/dto_template.txt',
        'patterns' => ['/{psr}/', '/{Module}/',  '/{Class}/',  '/{class}/', '/{classes}/'],
        'methods'  => [ 'setPsr',  'setModule', 'setUcClass', 'setLcClass',  'setClasses'],
        'values'   => [    'psr',     'module',      'class',      'class',       'class'],
    ],

    'hnd' => [
        'template' => APP_FOLDER . 'config/templates/bll/handler_template.txt',
        'patterns' => ['/{psr}/', '/{Module}/',  '/{Class}/',  '/{class}/', '/{classes}/'],
        'methods'  => [ 'setPsr',  'setModule', 'setUcClass', 'setLcClass',  'setClasses'],
        'values'   => [    'psr',     'module',      'class',      'class',       'class'],
    ],
];
