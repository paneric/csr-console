<?php

declare(strict_types=1);

use Paneric\CSRConsole\Service\BLL\ACCService;
use Paneric\CSRConsole\Service\BLL\ACRAPService;
use Paneric\CSRConsole\Service\BLL\ACROService;
use Paneric\CSRConsole\Service\BLL\ACUService;
use Paneric\CSRConsole\Service\BLL\ACDService;
use Paneric\CSRConsole\Service\BLL\BLLService;
use Paneric\CSRConsole\Service\BLL\CTRLService;
use Paneric\CSRConsole\Service\BLL\DTOService;
use Paneric\CSRConsole\Service\BLL\HNDService;
use Paneric\CSRConsole\Service\BLL\SRVService;
use Paneric\CSRConsole\Service\DAL\QRYService;
use Paneric\CSRConsole\Service\DAL\REPService;
use Paneric\CSRConsole\Service\GTW\EVTService;
use Paneric\CSRConsole\Service\GTW\LSTService;
use Paneric\CSRConsole\Service\GTW\GTWService;
use Paneric\CSRConsole\Service\IFC\IFCQService;
use Paneric\CSRConsole\Service\IFC\IFCRService;
use Paneric\CSRConsole\Service\IFC\IFCService;
use Psr\Container\ContainerInterface;

use Paneric\CSRConsole\Service\DAL\DAOService;
use Paneric\CSRConsole\Service\DAL\DAOStatementService;
use Paneric\CSRConsole\Service\DAL\ADAOService;
use Paneric\CSRConsole\Service\DAL\ADAOStatementService;

return [

    IFCRService::class => static function (ContainerInterface $container)
    {
        return new IFCRService(
            (string) $container->get('app_folder'),
            $container->get('ifcr')
        );
    },
    IFCQService::class => static function (ContainerInterface $container)
    {
        return new IFCQService(
            (string) $container->get('app_folder'),
            $container->get('ifcq')
        );
    },
    IFCService::class => static function (ContainerInterface $container)
    {
        return new IFCService(
            $container->get(IFCRService::class),
            $container->get(IFCQService::class)
        );
    },


    EVTService::class => static function (ContainerInterface $container)
    {
        return new EVTService(
            (string) $container->get('app_folder'),
            $container->get('evt')
        );
    },
    LSTService::class => static function (ContainerInterface $container)
    {
        return new LSTService(
            (string) $container->get('app_folder'),
            $container->get('lst')
        );
    },
    GTWService::class => static function (ContainerInterface $container)
    {
        return new GTWService(
            $container->get(EVTService::class),
            $container->get(LSTService::class)
        );
    },


    CTRLService::class => static function (ContainerInterface $container)
    {
        return new CTRLService(
            (string) $container->get('app_folder'),
            $container->get('ctrl')
        );
    },
    SRVService::class => static function (ContainerInterface $container)
    {
        return new SRVService(
            (string) $container->get('app_folder'),
            $container->get('srv')
        );
    },
    ACRAPService::class => static function (ContainerInterface $container)
    {
        return new ACRAPService(
            (string) $container->get('app_folder'),
            $container->get('acrap')
        );
    },
    ACROService::class => static function (ContainerInterface $container)
    {
        return new ACROService(
            (string) $container->get('app_folder'),
            $container->get('acro')
        );
    },
    ACCService::class => static function (ContainerInterface $container)
    {
        return new ACCService(
            (string) $container->get('app_folder'),
            $container->get('acc')
        );
    },
    ACUService::class => static function (ContainerInterface $container)
    {
        return new ACUService(
            (string) $container->get('app_folder'),
            $container->get('acu')
        );
    },
    ACDService::class => static function (ContainerInterface $container)
    {
        return new ACDService(
            (string) $container->get('app_folder'),
            $container->get('acd')
        );
    },
    DTOService::class => static function (ContainerInterface $container)
    {
        return new DTOService(
            (string) $container->get('app_folder'),
            $container->get('dto')
        );
    },
    HNDService::class => static function (ContainerInterface $container)
    {
        return new HNDService(
            (string) $container->get('app_folder'),
            $container->get('hnd')
        );
    },



    BLLService::class => static function (ContainerInterface $container)
    {
        return new BLLService(
            $container->get(CTRLService::class),
//            $container->get(SRVService::class),
            $container->get(ACCService::class),
            $container->get(ACUService::class),
            $container->get(ACDService::class),
            $container->get(ACRAPService::class),
            $container->get(ACROService::class),
            $container->get(HNDService::class),
            $container->get(DTOService::class)
        );
    },


    DAOStatementService::class => static function (ContainerInterface $container)
    {
        return new DAOStatementService();
    },
    DAOService::class => static function (ContainerInterface $container)
    {
        return new DAOService(
            (string) $container->get('app_folder'),
            $container->get('dao'),
            $container->get(DAOStatementService::class)
        );
    },
    ADAOStatementService::class => static function (ContainerInterface $container)
    {
        return new ADAOStatementService();
    },
    ADAOService::class => static function (ContainerInterface $container)
    {
        return new ADAOService(
            (string) $container->get('app_folder'),
            $container->get('adao'),
            $container->get(ADAOStatementService::class)
        );
    },
    REPService::class => static function (ContainerInterface $container)
    {
        return new REPService(
            (string) $container->get('app_folder'),
            $container->get('rep')
        );
    },
    QRYService::class => static function (ContainerInterface $container)
    {
        return new QRYService(
            (string) $container->get('app_folder'),
            $container->get('qry')
        );
    },
];
