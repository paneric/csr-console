<?php

declare(strict_types=1);

namespace {psr}BLL\{Module};

use {psr}BLL\{Module}\Action\{Class}AddAction;
use {psr}BLL\{Module}\Action\{Class}RemoveAction;
use {psr}BLL\{Module}\Action\{Class}ShowAllPaginatedAction;
use {psr}BLL\{Module}\Action\{Class}ShowOneByIdAction;
use {psr}BLL\{Module}\Action\{Class}EditAction;
use Paneric\CSRTriad\Controller\AppController;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class {Class}Controller extends AppController
{
    public function showAllPaginated(Response $response, {Class}ShowAllPaginatedAction $action): Response
    {
        return $this->render(
            $response,
            '@module/{class}/show_all_paginated.html.twig',
            $action->getAllPaginated()
        );
    }

    public function showOneById(Response $response, {Class}ShowOneByIdAction $action, int $id): Response
    {
        return $this->render(
            $response,
            '@module/{class}/show_one_by_id.html.twig',
            ['{class}' => $action->getOneById($id)]
        );
    }

    public function add(Request $request, Response $response, {Class}AddAction $action): Response
    {
        $result = $action->create($request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/{class}/{class}s/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/{class}/add.html.twig',
            $result
        );
    }

    public function edit(Request $request, Response $response, {Class}EditAction $action, int $id): Response
    {
        $result = $action->update($id, $request);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/{class}/{class}s/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/{class}/edit.html.twig',
            $result
        );
    }

    public function remove(Request $request, Response $response, {Class}RemoveAction $action, int $id): Response
    {
        $result = $action->delete($request, $id);
        if ($result === null) {
            return $this->redirect(
                $response,
                '/{class}/{class}s/show',
                200
            );
        }

        return $this->render(
            $response,
            '@module/{class}/remove.html.twig',
            ['id' => $id]
        );
    }
}
