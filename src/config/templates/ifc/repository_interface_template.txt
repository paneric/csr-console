<?php

declare(strict_types=1);

namespace {psr}Interfaces\{Module};

use {psr}Interfaces\RepositoryInterface;

interface {Class}RepositoryInterface extends RepositoryInterface {}
