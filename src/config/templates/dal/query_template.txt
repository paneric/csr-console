<?php

declare(strict_types=1);

namespace {psr}DAL\{Module};

use {psr}Interfaces\{Module}\{Class}QueryInterface;
use {psr}DAL\{Module}\{Class}ADAO;

use Paneric\DBAL\Manager;
use Paneric\DBAL\Query;

class {Class}Query extends Query implements {Class}QueryInterface
{
    public function __construct(Manager $manager)
    {
        parent::__construct($manager);

        $this->adaoClass = {Class}ADAO::class;
    }

    public function queryBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array
    {
        return [];
    }
}
