<?php

declare(strict_types=1);

namespace Paneric\CSRConsole\Service;

use RuntimeException;

trait ServiceTrait2
{
    /* (1.1.) */
    protected function createClassFromTemplate(
        string $psr,
        string $module,
        string $class
    ): ?int {
        $classFileAsString = $this->setClassFileAsString(
            $psr,
            $module,
            $class
        );

        $result = file_put_contents (
            $this->setClassFilePath($module, $class),
            $classFileAsString
        );

        if ($result === false) {
            return null;
        }

        return $result;
    }

    /* (1.1.1.) */
    protected function setClassFileAsString(
        string $psr,
        string $module,
        string $class
    ): string {
        return preg_replace(
            $this->settings['patterns'],
            $this->setParams($psr, $module, $class),
            file_get_contents ($this->settings['template'])
        );
    }

    /* (1.1.1.1.) */
    protected function setParams(
        string $psr,
        string $module,
        string $class
    ): array {
        $values = [];

        foreach ($this->settings['methods'] as $key => $method) {
            $paramName = $this->settings['values'][$key];

            $values[$key] = $this->$method(${$paramName});
        }

        if ($values === []) {
            throw new RuntimeException('Paneric\\Array of values is not defined');
        }

        return $values;
    }
}
