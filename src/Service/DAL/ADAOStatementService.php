<?php

declare(strict_types=1);

namespace Paneric\CSRConsole\Service\DAL;

class ADAOStatementService
{
    use DALStatementServiceTrait;

    /* (1.) */
    public function setStatementsFromTemplates(
        string $attributes,
        string $attributesTypes,
        string $namespaces,
        array $settings
    ): array
    {
        $attributes = explode(',', preg_replace('/\s+/', '', $attributes));

        $attributesTypes = explode(',', preg_replace("/\s+/", '', $attributesTypes));

        $namespaces = explode(',', preg_replace("/\s+/", '', $namespaces));

        $statements = $this->prepareStatements($settings, $attributes);

        $settings = $this->transformStatementsSettings($settings, $attributes);

        $initialValues = $this->initSettingsValues($settings, $attributes, $attributesTypes, $namespaces);

        $settings = $this->setSettingsValues($settings, $initialValues);

        return array_values(
            $this->injectValuesIntoStatements($settings, $statements)
        );
    }

    protected function setUsesAsString(int $key, string $statement): string
    {
        return str_replace (
                ['{Namespace}', '{Type}'],
                ['{Namespace' . $key . '}', '{Type' . $key . '}'],
                $statement
            ) . PHP_EOL;
    }

    protected function setAttributesAsString(int $key, string $statement): string
    {
        return str_replace (
                ['{Type}', '{attribute}'],
                ['{Type' . $key . '}', '{attribute' . $key . '}'],
                $statement
            ) . PHP_EOL;
    }

    protected function setConstructorAsString(int $key, string $statement): string
    {
        return str_replace (
                ['{attribute}', '{Type}'],
                ['{attribute' . $key . '}', '{Type' . $key . '}'],
                $statement
            ) . PHP_EOL;
    }
}
