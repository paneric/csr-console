<?php

declare(strict_types=1);

namespace Paneric\CSRConsole\Service\DAL;

use Paneric\CSRConsole\Service\AbstractService;
use Paneric\CSRConsole\Service\ServiceTrait;
use Paneric\CSRConsole\Service\ServiceTrait2;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Output\OutputInterface;

class QRYService extends AbstractService
{
    use ServiceTrait;
    use ServiceTrait2;

    protected $appLayer;

    public function __construct(string $appFolder, array $settings)
    {
        parent::__construct($appFolder, $settings);

        $this->classType = 'Query';

        $this->appLayer = 'DAL';
    }

    /* 1. */
    public function  createClass(
        OutputInterface $output,
        string $psr,
        string $module,
        string $class
    ): void {
        if (!$this->createClassFromTemplate($psr, $module, $class)) {
            $this->setErrorOutput($output, $psr, $module, $class);
        }

        $this->setOutput($output, $psr, $module, $class);
    }

    /* (1.1.2.) */
    protected function setClassFilePath(string $module, string $class): string
    {
        return sprintf(
            '%s/%s%s.php',
            $this->setClassFileDirectory($module),
            ucfirst($class),
            $this->classType
        );
    }

    /* 1.3. */
    protected function setOutput(OutputInterface $output, string $psr, string $module, string $class): void
    {
        $output->getFormatter()->setStyle(
            'title',
            new OutputFormatterStyle('green', 'white', ['bold'])
        );

        $output->writeln([
            '',
            '<title>                                                                          </>',
            '<title>  QUERY:                                                                  </>',
            '<title>                                                                          </>',
            '',
            sprintf(
                '<options=bold>  %s\\DAL\\%s\\%sQuery is created with success. </>',
                $psr,
                $module,
                $class
            ),
            ''
        ]);
    }

    /* 1.2.1. */
    protected function setErrorMessage(string $psr, string $module, string $class): string
    {
        return sprintf(
            '<options=bold>  %s\\DAL\\%s\\%sQuery creation failure. </>',
            $psr,
            $module,
            $class
        );
    }
}
