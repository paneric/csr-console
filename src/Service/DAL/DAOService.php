<?php

declare(strict_types=1);

namespace Paneric\CSRConsole\Service\DAL;

use Paneric\CSRConsole\Service\AbstractService;
use Paneric\CSRConsole\Service\ServiceTrait;
use RuntimeException;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Output\OutputInterface;

class DAOService extends AbstractService
{
    use ServiceTrait;

    protected $statementService;
    protected $prefix;
    protected $appLayer;

    public function __construct(
        string $appFolder,
        array $settings,
        DAOStatementService $statementService
    ) {
        parent::__construct($appFolder, $settings);

        $this->classType = 'DAO';

        $this->appLayer = 'DAL';

        $this->statementService = $statementService;
    }

    /* 1. */
    public function createClass(
        OutputInterface $output,
        string $psr,
        string $module,
        string $class,
        string $attributes,
        string $attributesTypes,
        string $prefix
    ): void {
        $this->prefix = $prefix;

        if (!$this->createClassFromTemplate($psr, $module, $class, $attributes, $attributesTypes)) {
            $this->setErrorOutput($output, $psr, $module, $class);
        }

        $this->setOutput($output, $psr, $module, $class);
    }

    /* (1.1.) */
    protected function createClassFromTemplate(
        string $psr,
        string $module,
        string $class,
        string $attributes = null,
        string $attributesTypes = null
    ): ?int {
        $classFileAsString = $this->setClassFileAsString(
            $psr,
            $module,
            $class,
            $attributes,
            $attributesTypes
        );

        $statementsAsStrings = $this->statementService->setStatementsFromTemplates(
            $attributes,
            $attributesTypes,
            $this->settings['statements']
        );

        $classFileAsString = preg_replace(
            $this->settings['statements_patterns'],
            $statementsAsStrings,
            $classFileAsString
        );

        $result = file_put_contents (
            $this->setClassFilePath($module, $class),
            $classFileAsString
        );

        if ($result === false) {
            return null;
        }

        return $result;
    }

    /* (1.1.1.) */
    protected function setClassFileAsString(
        string $psr,
        string $module,
        string $class,
        string $attributes = null,
        string $attributesTypes = null
    ): string {
        return preg_replace(
            $this->settings['patterns'],
            $this->setParams($psr, $module, $class, $this->prefix, $attributes, $attributesTypes),
            file_get_contents ($this->settings['template'])
        );
    }

    /* (1.1.1.1.) */
    protected function setParams(
        string $psr,
        string $module,
        string $class,
        string $prefix,
        string $attributes,
        string $attributesTypes
    ): array {
        $values = [];

        foreach ($this->settings['methods'] as $key => $method) {
            $paramName = $this->settings['values'][$key];

            $values[$key] = $this->$method(${$paramName});
        }

        if ($values === []) {
            throw new RuntimeException('Paneric\\Array of values is not defined');
        }

        return $values;
    }

    /* (1.1.2.) */
    protected function setClassFilePath(string $module, string $class): string
    {
        return sprintf(
            '%s/%sDAO.php',
            $this->setClassFileDirectory($module),
            $class
        );
    }

    /* 1.2.1. */
    protected function setErrorMessage(string $psr, string $module, string $class): string
    {
        return sprintf(
            '<options=bold>  %s\\DAL\\%s\\%sDAO creation failure. </>',
            $psr,
            $module,
            $class
        );
    }

    /* 1.3. */
    protected function setOutput(OutputInterface $output, string $psr, string $module, string $class): void
    {
        $output->getFormatter()->setStyle(
            'title',
            new OutputFormatterStyle('green', 'white', ['bold'])
        );

        $output->writeln([
            '',
            '<title>                                                                          </>',
            '<title>  DAO:                                                                    </>',
            '<title>                                                                          </>',
            '',
            sprintf(
                '<options=bold>  %s\\DAL\\%s\\%sDAO is created with success. </>',
                $psr,
                $module,
                $class
            ),
            ''
        ]);
    }
}
