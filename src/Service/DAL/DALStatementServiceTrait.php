<?php

declare(strict_types=1);

namespace Paneric\CSRConsole\Service\DAL;

use RuntimeException;

trait DALStatementServiceTrait
{
    /* (1.1.) */
    protected function prepareStatements(
        array $settings,
        array $attributes
    ): array {
        $preparedStatements = [];

        foreach ($attributes as $key => $attribute) {
            foreach ($settings as $keySettings => $setting) {
                $preparedStatements[$keySettings] = $preparedStatements[$keySettings] ?? '';
                $preparedStatements[$keySettings] .= $this->{'set' . ucfirst($keySettings) . 'AsString'}(
                    $key,
                    $settings[$keySettings]['template']
                );
            }
        }

        return $preparedStatements;
    }

    /* (1.2.) */
    protected function transformStatementsSettings(array $statements, array $attributes): array
    {
        $attributesNumber = count($attributes);

        foreach ($statements as $keyStatement => $statement) {
            $valuesNumber = count($statement['values']);

            $patterns = [];
            $methods = [];
            $values = [];

            for ($iValue = 0; $iValue < $valuesNumber; $iValue++) {
                $pattern = $statement['patterns'][$iValue];
                $method = $statement['methods'][$iValue];
                $value = $statement['values'][$iValue];


                for ($iAttribute = 0; $iAttribute < $attributesNumber; $iAttribute++) {
                    $patterns[] = str_replace ( '}/', $iAttribute . '}/', $pattern);
                    $methods[] = $method;
                    $values[] = $value . $iAttribute;
                }
            }

            $statements[$keyStatement]['patterns'] = $patterns;
            $statements[$keyStatement]['methods'] = $methods;
            $statements[$keyStatement]['values'] = $values;
        }

        return $statements;
    }

    /* (1.3.) */
    protected function initSettingsValues(array $settings, array $attributes, array $types, array $namespaces = null): array
    {
        $initialValues = [];

        foreach ($settings as $key => $setting) {
            foreach ($setting['values'] as $idx => $value) {
                $alpha = preg_replace('/[0-9]+/', '', $value) . 's';
                $num = preg_replace('/[^0-9]/', '', $value );

                $initialValues[$key][$idx] = $$alpha[$num];
            }
        }

        if ($initialValues === []) {
            throw new RuntimeException('Paneric\\Array of params is not defined');
        }

        return $initialValues;
    }

    /* (1.4.) */
    protected function setSettingsValues(array $settings, array $initialValues): array {

        foreach ($settings as $keySetting => $setting) {
            $values = [];

            foreach ($setting['methods'] as $keyMethod => $method) {
                $values[$keyMethod] = $this->$method($initialValues[$keySetting][$keyMethod]);
            }

            if ($values === []) {
                throw new RuntimeException('Paneric\\Array of values is not defined');
            }

            $settings[$keySetting]['values'] = $values;
        }

        return $settings;
    }

    /* (1.5.) */
    protected function injectValuesIntoStatements(array $settings, array $statements): array
    {
        foreach ($statements as $keyStatement => $statement) {
            $statements[$keyStatement] = preg_replace(
                $settings[$keyStatement]['patterns'],
                $settings[$keyStatement]['values'],
                $statement
            );
        }

        return $statements;
    }

    protected function setGettersAsString(int $key, string $statement): string
    {
        return str_replace (
                ['{Attribute}', '{Type}', '{attribute}'],
                ['{Attribute' . $key . '}', '{Type' . $key . '}', '{attribute' . $key . '}'],
                $statement
            ) . PHP_EOL;
    }

    protected function setSettersAsString(int $key, string $statement): string
    {
        return str_replace (
                ['{Attribute}', '{Type}', '{attribute}'],
                ['{Attribute' . $key . '}', '{Type' . $key . '}', '{attribute' . $key . '}'],
                $statement
            ) . PHP_EOL;
    }

    protected function setUcAttribute(string $attribute): string
    {
        return ucfirst($attribute);
    }

    protected function setLcAttribute(string $attribute): string
    {
        return lcfirst($attribute);
    }

    protected function setLcType(string $type): string
    {
        return lcfirst($type);
    }

    protected function setUcType(string $type): string
    {
        return ucfirst($type);
    }

    protected function setUcPsr(string $psr): string
    {
        return ucfirst($psr) . '\\';
    }

    protected function setUcNamespace(string $namespace): string
    {
        return ucfirst($namespace);
    }

    protected function setLcSnakeAttribute(string $name): string
    {
        return strtolower(preg_replace('/(?<=\\w)(?=[A-Z])/','_$1', $name));
    }
}
