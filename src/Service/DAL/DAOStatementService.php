<?php

declare(strict_types=1);

namespace Paneric\CSRConsole\Service\DAL;

class DAOStatementService
{
    use DALStatementServiceTrait;

    /* (1.) */
    public function setStatementsFromTemplates(
        string $attributes,
        string $attributesTypes,
        array $settings
    ): array
    {
        $attributes = explode(',', preg_replace('/\s+/', '', $attributes));

        $attributesTypes = explode(',', preg_replace("/\s+/", '', $attributesTypes));

        $statements = $this->prepareStatements($settings, $attributes);

        $settings = $this->transformStatementsSettings($settings, $attributes);

        $initialValues = $this->initSettingsValues($settings, $attributes, $attributesTypes);

        $settings = $this->setSettingsValues($settings, $initialValues);

        return array_values(
            $this->injectValuesIntoStatements($settings, $statements)
        );
    }

    protected function setAttributesAsString(int $key, string $statement): string
    {
        return str_replace (
                ['{type}', '{attribute}'],
                ['{type' . $key . '}', '{attribute' . $key . '}'],
                $statement
            ) . PHP_EOL;
    }
}
