<?php

declare(strict_types=1);

namespace Paneric\CSRConsole\Service\BLL;

use Symfony\Component\Console\Output\OutputInterface;

class BLLService
{
    protected $ctrlService;
//    protected $srvService;
    protected $accService;
    protected $acuService;
    protected $acdService;
    protected $acrapService;
    protected $acroService;
    protected $hndService;
    protected $dtoService;

    public function __construct(
        CTRLService $ctrlService,
//        SRVService $srvService,
        ACCService $accService,
        ACUService $acuService,
        ACDService $acdService,
        ACRAPService $acrapService,
        ACROService $acroService,
        HNDService $hndService,
        DTOService $dtoService
    ) {
        $this->ctrlService = $ctrlService;
//        $this->srvService = $srvService;
        $this->accService = $accService;
        $this->acuService = $acuService;
        $this->acdService = $acdService;
        $this->acrapService = $acrapService;
        $this->acroService = $acroService;
        $this->hndService = $hndService;
        $this->dtoService = $dtoService;
    }

    /* 1. */
    public function createClass(
        OutputInterface $output,
        string $class,
        string $module,
        string $psr
    ): void {
        $this->ctrlService->createClass($output, $class, $module, $psr);
//        $this->srvService->createClass($output, $class, $module, $psr);
        $this->accService->createClass($output, $class, $module, $psr);
        $this->acuService->createClass($output, $class, $module, $psr);
        $this->acdService->createClass($output, $class, $module, $psr);
        $this->acrapService->createClass($output, $class, $module, $psr);
        $this->acroService->createClass($output, $class, $module, $psr);
        $this->hndService->createClass($output, $class, $module, $psr);
        $this->dtoService->createClass($output, $class, $module, $psr);
    }
}
