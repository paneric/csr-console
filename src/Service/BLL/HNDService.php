<?php

declare(strict_types=1);

namespace Paneric\CSRConsole\Service\BLL;

use Paneric\CSRConsole\Service\AbstractService;
use Paneric\CSRConsole\Service\ServiceTrait;
use Paneric\CSRConsole\Service\ServiceTrait2;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Output\OutputInterface;

class HNDService extends AbstractService
{
    use ServiceTrait;
    use ServiceTrait2;

    protected $appLayer;

    public function __construct(string $appFolder, array $settings)
    {
        parent::__construct($appFolder, $settings);

        $this->classType = 'Handler';

        $this->appLayer = 'BLL';
    }

    /* 1. */
    public function  createClass(
        OutputInterface $output,
        string $psr,
        string $module,
        string $class
    ): void {
        if (!$this->createClassFromTemplate($psr, $module, $class)) {
            $this->setErrorOutput($output, $psr, $module, $class);
        }

        $this->setOutput($output, $psr, $module, $class);
    }

    /* (1.1.2.) */
    protected function setClassFilePath(string $module, string $class): string
    {
        return sprintf(
            '%s/%s%s.php',
            $this->setClassFileDirectoryBLLHandler($module),
            ucfirst($class),
            $this->classType
        );
    }

    /* 1.3. */
    protected function setOutput(OutputInterface $output, string $psr, string $module, string $class): void
    {
        $output->getFormatter()->setStyle(
            'title',
            new OutputFormatterStyle('green', 'white', ['bold'])
        );

        $output->writeln([
            '',
            '<title>                                                                          </>',
            '<title>  HANDLER:                                                                </>',
            '<title>                                                                          </>',
            '',
            sprintf(
                '<options=bold>  %s\\%s\\%s\\%s%s is created with success. </>',
                $psr,
                $this->appLayer,
                $module,
                $class,
                $this->classType
            ),
            ''
        ]);
    }

    /* 1.2.1. */
    protected function setErrorMessage(string $psr, string $module, string $class): string
    {
        return sprintf(
            '<fg=red;options=bold>  %s\\%s\\%s\\%s%s creation failure. </>',
            $psr,
            $this->appLayer,
            $module,
            $class,
            $this->classType
        );
    }
}
