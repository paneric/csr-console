<?php

declare(strict_types=1);

namespace Paneric\CSRConsole\Service\DAL;

use Paneric\CSRConsole\Service\AbstractService;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Output\OutputInterface;

class REPIService extends AbstractService
{
    public function __construct(string $appFolder, array $settings)
    {
        parent::__construct($appFolder, $settings);

        $this->classType = 'Repository';
    }

    /* 1.3. */
    protected function setOutput(OutputInterface $output, string $psr, string $module, string $class): void
    {
        $output->getFormatter()->setStyle(
            'title',
            new OutputFormatterStyle('green', 'white', ['bold'])
        );

        $output->writeln([
            '',
            '<title>                                                                          </>',
            '<title>  REPOSITORY INTERFACE:                                                   </>',
            '<title>                                                                          </>',
            '',
            sprintf(
                '<options=bold>  %s\\%s\\Repository\\Interfaces\\%sRepositoryInterface is created with success. </>',
                $psr,
                $module,
                $class
            ),
            ''
        ]);
    }

    /* 1.2.1. */
    protected function setErrorMessage(string $psr, string $module, string $class): string
    {
        return sprintf(
            '<fg=red;options=bold>  %s\\%s\\Repository\\Interfaces\\%sRepositoryInterface creation failure. </>',
            $psr,
            $module,
            $class
        );
    }

    /* (1.1.2.) */
    protected function setClassFilePath(string $module, string $class): string
    {
        return sprintf(
            '%s/%sRepositoryInterface.php',
            $this->setClassFileDirectory($module),
            $class
        );
    }

    /* (1.1.2.1.) */
    protected function setClassFileDirectory(string $module): string
    {
        if ($module !== '') {
            $module .= '/';
        }

        $classFileDirectory = sprintf(
            '%s%s%s/%s',
            $this->appFolder,
            ucfirst($module),
            $this->classType,
            'Interfaces'
        );

        if (!file_exists($classFileDirectory)) {
            if (
                !mkdir($classFileDirectory, 0777, true)
                && !is_dir($classFileDirectory)
            ) {
                throw new RuntimeException(sprintf(
                    'Directory "%s" was not created',
                    $classFileDirectory
                ));
            }
        }

        return $classFileDirectory;
    }
}