<?php

declare(strict_types=1);

namespace Paneric\CSRConsole\Service\BLL;

class ACCService extends ACNService
{
    public function __construct(string $appFolder, array $settings)
    {
        parent::__construct($appFolder, $settings, 'Add');
    }
}
