<?php

declare(strict_types=1);

namespace Paneric\CSRConsole\Service;

use RuntimeException;

trait ServiceTrait
{
    /* (1.1.2.1.) */
    protected function setClassFileDirectory(string $module): string
    {
        if ($module !== '') {
            $module .= '/';
        }

        $classFileDirectory = sprintf(
            '%s%s%s',
            $this->appFolder,
            $this->appLayer . '/',
            ucfirst($module)
        );

        if (!file_exists($classFileDirectory)) {
            if (
                !mkdir($classFileDirectory, 0777, true)
                && !is_dir($classFileDirectory)
            ) {
                throw new RuntimeException(sprintf(
                    'Directory "%s" was not created',
                    $classFileDirectory
                ));
            }
        }

        return $classFileDirectory;
    }

    /* (1.1.2.1.) */
    protected function setClassFileDirectoryBLLHandler(string $module): string
    {
        if ($module !== '') {
            $module .= '/';
        }

        $classFileDirectory = sprintf(
            '%s%s%s%s',
            $this->appFolder,
            $this->appLayer . '/',
            ucfirst($module),
            'Handler/'
        );

        if (!file_exists($classFileDirectory)) {
            if (
                !mkdir($classFileDirectory, 0777, true)
                && !is_dir($classFileDirectory)
            ) {
                throw new RuntimeException(sprintf(
                    'Directory "%s" was not created',
                    $classFileDirectory
                ));
            }
        }

        return $classFileDirectory;
    }

    /* (1.1.2.1.) */
    protected function setClassFileDirectoryBLLAction(string $module): string
    {
        if ($module !== '') {
            $module .= '/';
        }

        $classFileDirectory = sprintf(
            '%s%s%s%s',
            $this->appFolder,
            $this->appLayer . '/',
            ucfirst($module),
            'Action/'
        );

        if (!file_exists($classFileDirectory)) {
            if (
                !mkdir($classFileDirectory, 0777, true)
                && !is_dir($classFileDirectory)
            ) {
                throw new RuntimeException(sprintf(
                    'Directory "%s" was not created',
                    $classFileDirectory
                ));
            }
        }

        return $classFileDirectory;
    }
}
