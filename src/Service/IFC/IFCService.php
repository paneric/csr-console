<?php

declare(strict_types=1);

namespace Paneric\CSRConsole\Service\IFC;

use Symfony\Component\Console\Output\OutputInterface;

class IFCService
{
    protected $ifcrService;
    protected $ifcqService;

    public function __construct(
        IFCRService $ifcrService,
        IFCQService $ifcqService
    ) {
        $this->ifcrService = $ifcrService;
        $this->ifcqService = $ifcqService;
    }

    /* 1. */
    public function createClass(
        OutputInterface $output,
        string $class,
        string $module,
        string $psr
    ): void {
        $this->ifcrService->createClass($output, $class, $module, $psr);
        $this->ifcqService->createClass($output, $class, $module, $psr);
    }
}
