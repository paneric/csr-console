<?php

declare(strict_types=1);

namespace Paneric\CSRConsole\Service;

use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Output\OutputInterface;

abstract class AbstractService
{
    protected $appFolder;
    protected $settings;

    protected $classType;

    public function __construct(string $appFolder, array $settings)
    {
        $this->appFolder = $appFolder;

        $this->settings = $settings;
    }

    /* (1.2.) */
    protected function setErrorOutput(OutputInterface $output, string $psr, string $module, string $class): void
    {
        $output->getFormatter()->setStyle(
            'title',
            new OutputFormatterStyle('red', 'white', ['bold'])
        );

        $output->writeln([
            '',
            '<title>                                                                          </>',
            '<title>  ERROR:                                                                  </>',
            '<title>                                                                          </>',
            '',
            $this->setErrorMessage($psr, $module, $class),
            ''
        ]);
    }

    /* 1.2.1. */
    abstract protected function setErrorMessage(string $psr, string $module, string $class): string;

    /* 1.3. */
    abstract protected function setOutput(OutputInterface $output, string $psr, string $module, string $class): void;

    protected function setPsr(string $psr): string
    {
        return $psr . '\\';
    }

    protected function setModule(string $module): string
    {
        if ($module !== '') {
            return ucfirst($module);
        }

        return $module;
    }

    protected function setModuleRoute(string $module): string
    {
        $moduleRoute = lcfirst($module);

        if (strpos($module, '_') !== false) {
            $moduleRoute = str_replace(' ', '', ucwords(str_replace('_', ' ', $module)));
            $moduleRoute[0] = lcfirst($moduleRoute[0]);
        }

        return $moduleRoute;
    }

    protected function setUcNamespace(string $namespace): string
    {
        return ucfirst($namespace);
    }

    protected function setUcClass(string $class): string
    {
        return ucfirst($class);
    }

    protected function setLcClass(string $class): string
    {
        return lcfirst($class);
    }

    protected function setLcPrefix(string $prefix): string
    {
        return lcfirst($prefix);
    }

    protected function setClasses(string $class): string
    {
        return lcfirst(substr($class, -1) === 's' ? $class . 'es' : $class . 's');
    }
}
