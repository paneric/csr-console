<?php

declare(strict_types=1);

namespace Paneric\CSRConsole\Service\GTW;

use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Output\OutputInterface;

class EVTService extends GService
{
    public function __construct(string $appFolder, array $settings)
    {
        parent::__construct($appFolder, $settings);

        $this->classType = 'Event';

        $this->appLayer = 'Gateway';
    }

    /* 1.3. */
    protected function setOutput(OutputInterface $output, string $psr, string $module, string $class): void
    {
        $output->getFormatter()->setStyle(
            'title',
            new OutputFormatterStyle('green', 'white', ['bold'])
        );

        $output->writeln([
            '',
            '<title>                                                                          </>',
            '<title>  EVENT:                                                                  </>',
            '<title>                                                                          </>',
            '',
            sprintf(
                '<options=bold>  %s\\%s\\%s\\%s%s is created with success. </>',
                $psr,
                $this->appLayer,
                $module,
                $class,
                $this->classType
            ),
            ''
        ]);
    }

    /* 1.2.1. */
    protected function setErrorMessage(string $psr, string $module, string $class): string
    {
        return sprintf(
            '<options=bold>  %s\\%s\\%s\\%s%s failure. </>',
            $psr,
            $this->appLayer,
            $module,
            $class,
            $this->classType
        );
    }
}
