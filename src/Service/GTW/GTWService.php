<?php

declare(strict_types=1);

namespace Paneric\CSRConsole\Service\GTW;

use Symfony\Component\Console\Output\OutputInterface;

class GTWService
{
    protected $evtService;
    protected $lstService;

    public function __construct(
        EVTService $evtService,
        LstService $lstService
    ) {
        $this->evtService = $evtService;
        $this->lstService = $lstService;
    }

    /* 1. */
    public function createClass(
        OutputInterface $output,
        string $class,
        string $module,
        string $psr
    ): void {
        $this->evtService->createClass($output, $class, $module, $psr);
        $this->lstService->createClass($output, $class, $module, $psr);
    }
}
