<?php

declare(strict_types=1);

namespace Paneric\CSRConsole\Command;

use Paneric\CSRConsole\Service\DAL\ADAOService;
use Paneric\CSRConsole\Service\DAL\QRYService;
use Paneric\CSRConsole\Service\DAL\REPService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Psr\Container\ContainerInterface;
use Paneric\CSRConsole\Service\DAL\DAOService;

class DALCreateCommand extends Command
{
    protected static $defaultName = 'dal';

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription('Creates Data Access Layer classes.')
            ->setHelp('This command allows you to create Data Access Layer classes.')
            ->addOption(
                'mode',
                null,
                InputOption::VALUE_REQUIRED,
                'Choice of required class.',
                'dao'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $service = null;
        $attributes = [];
        $attributesTypes = [];
        $prefix = null;

        $mode = $input->getOption('mode');

        $questionHelper = $this->getHelper('question');
        $class = $questionHelper->ask(
            $input,
            $output,
            new Question('Class name: ')
        );
        $module = $questionHelper->ask(
            $input,
            $output,
            new Question('Module name: ')
        );
        $psr = $questionHelper->ask(
            $input,
            $output,
            new Question('psr-4: ')
        );

        if ($mode === 'dao' || $mode === 'dal') {
            $prefix = $questionHelper->ask(
                $input,
                $output,
                new Question('DB table field prefix: ')
            );
            $attributes = $questionHelper->ask(
                $input,
                $output,
                new Question('DAO attributes names: ')
            );

            $attributesTypes = $questionHelper->ask(
                $input,
                $output,
                new Question('DAO attributes types: ')
            );
        }

        if ($mode === 'dao') {
            $service = $this->executeDAO($output, $psr, $module, $class, $attributes, $attributesTypes, $prefix);
        }

        if ($mode === 'adao') {
            $namespace = $questionHelper->ask(
                $input,
                $output,
                new Question('Namespaces of aggregated DAOs: ')
            );
            $attributesTypes = $questionHelper->ask(
                $input,
                $output,
                new Question('Types aggregated DAOs: ')
            );
            $attributes = $questionHelper->ask(
                $input,
                $output,
                new Question('Names of aggregated DAOs: ')
            );

            $service = $this->executeADAO($output, $psr, $module, $class, $attributes, $attributesTypes, $namespace);
        }

        if ($mode === 'rep') {
            $service = $this->container->get(REPService::class);
            $service->createClass($output, $psr, $module, $class);

            return 0;
        }

        if ($mode === 'qry') {
            $service = $this->container->get(QRYService::class);
            $service->createClass($output, $psr, $module, $class);

            return 0;
        }

        if ($mode === 'dal') {
            $this->executeDAL($output, $psr, $module, $class, $attributes, $attributesTypes, $prefix);

            return 0;
        }

        if ($service === null) {
            $this->setModeErrorOutput($output, $mode);

            return 0;
        }

        return 0;
    }

    protected function executeDAO(
        OutputInterface $output,
        string $psr,
        string $module,
        string $class,
        string $attributes,
        string $attributesTypes,
        string $prefix
    ): bool {
        $service = $this->container->get(DAOService::class);
        $service->createClass($output, $psr, $module, $class, $attributes, $attributesTypes, $prefix);

        return true;
    }

    protected function executeADAO(
        OutputInterface $output,
        string $psr,
        string $module,
        string $class,
        string $attributes,
        string $attributesTypes,
        string $namespace
    ): bool {
        $service = $this->container->get(ADAOService::class);
        $service->createClass($output, $psr, $module, $class, $attributes, $attributesTypes, $namespace);

        return true;
    }

    protected function executeDAL(
        OutputInterface $output,
        string $psr,
        string $module,
        string $class,
        string $attributes,
        string $attributesTypes,
        string $prefix
    ): bool {
        $service = $this->container->get(DAOService::class);
        $service->createClass($output, $psr, $module, $class, $attributes, $attributesTypes, $prefix);

        $service = $this->container->get(REPService::class);
        $service->createClass($output, $psr, $module, $class);

        $service = $this->container->get(QRYService::class);
        $service->createClass($output, $psr, $module, $class);

        return true;
    }

    protected function setModeErrorOutput(OutputInterface $output, string $mode): void
    {
        $output->getFormatter()->setStyle(
            'title',
            new OutputFormatterStyle('red', 'white', ['bold'])
        );

        $output->writeln([
            '',
            '<title>                                                                          </>',
            '<title>  ERROR:                                                                  </>',
            '<title>                                                                          </>',
            '',
            $this->setModeErrorMessage($mode),
            ''
        ]);
    }

    protected function setModeErrorMessage(string $mode): string
    {
        return sprintf(
            '<fg=red;options=bold> Incorrect mode value: %s. </>',
            $mode
        );
    }
}
