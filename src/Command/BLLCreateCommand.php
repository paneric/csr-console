<?php

declare(strict_types=1);

namespace Paneric\CSRConsole\Command;

use Paneric\CSRConsole\Service\BLL\ACCService;
use Paneric\CSRConsole\Service\BLL\ACDService;
use Paneric\CSRConsole\Service\BLL\ACRAPService;
use Paneric\CSRConsole\Service\BLL\ACROService;
use Paneric\CSRConsole\Service\BLL\ACUService;
use Paneric\CSRConsole\Service\BLL\CTRLService;
use Paneric\CSRConsole\Service\BLL\DTOService;
use Paneric\CSRConsole\Service\BLL\HNDService;
use Paneric\CSRConsole\Service\BLL\SRVService;
use Paneric\CSRConsole\Service\BLL\BLLService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Psr\Container\ContainerInterface;

class BLLCreateCommand extends Command
{
    protected static $defaultName = 'bll';

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription('Creates Business Logic Layer classes.')
            ->setHelp('This command allows you to create Business Logic Layer classes.')
            ->addOption(
                'mode',
                null,
                InputOption::VALUE_REQUIRED,
                'Choice of required class.',
                'ctrl'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $mode = $input->getOption('mode');

        $questionHelper = $this->getHelper('question');
        $class = $questionHelper->ask(
            $input,
            $output,
            new Question('Class name: ')
        );
        $module = $questionHelper->ask(
            $input,
            $output,
            new Question('Module name: ')
        );
        $psr = $questionHelper->ask(
            $input,
            $output,
            new Question('psr-4: ')
        );

        $this->executeMode($output, $mode, $psr, $module, $class);

        return 0;
    }

    protected function executeMode(
        OutputInterface $output,
        string $mode,
        string $psr,
        string $module,
        string $class
    ): bool {
        $service = null;

        if ($mode === 'ctrl') {
            $service = $this->container->get(CTRLService::class);
        }
//        if ($mode === 'srv') {
//            $service = $this->container->get(SRVService::class);
//        }
        if ($mode === 'acrap') {
            $service = $this->container->get(ACRAPService::class);
        }
        if ($mode === 'acro') {
            $service = $this->container->get(ACROService::class);
        }
        if ($mode === 'acc') {
            $service = $this->container->get(ACCService::class);
        }
        if ($mode === 'acu') {
            $service = $this->container->get(ACUService::class);
        }
        if ($mode === 'acd') {
            $service = $this->container->get(ACDService::class);
        }
        if ($mode === 'dto') {
            $service = $this->container->get(DTOService::class);
        }
        if ($mode === 'hnd') {
            $service = $this->container->get(HNDService::class);
        }
        if ($mode === 'bll') {
            $service = $this->container->get(BLLService::class);
        }

        if ($service === null) {
            $this->setModeErrorOutput($output, $mode);

            return true;
        }

        $service->createClass($output, $psr, $module, $class);

        return true;
    }

    protected function setModeErrorOutput(OutputInterface $output, string $mode): void
    {
        $output->getFormatter()->setStyle(
            'title',
            new OutputFormatterStyle('red', 'white', ['bold'])
        );

        $output->writeln([
            '',
            '<title>                                                                          </>',
            '<title>  ERROR:                                                                  </>',
            '<title>                                                                          </>',
            '',
            $this->setModeErrorMessage($mode),
            ''
        ]);
    }

    protected function setModeErrorMessage(string $mode): string
    {
        return sprintf(
            '<fg=red;options=bold> Incorrect mode value: %s. </>',
            $mode
        );
    }
}
