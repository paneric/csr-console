<?php

declare(strict_types=1);

namespace Paneric\CSRConsole\Command;

use Paneric\CSRConsole\Service\GTW\EVTService;
use Paneric\CSRConsole\Service\GTW\GTWService;
use Paneric\CSRConsole\Service\GTW\LSTService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Psr\Container\ContainerInterface;

class GTWCreateCommand extends Command
{
    protected static $defaultName = 'gtw';

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription('Creates Gateway Layer classes.')
            ->setHelp('This command allows you to create Gateway Layer classes.')
            ->addOption(
                'mode',
                null,
                InputOption::VALUE_REQUIRED,
                'Choice of required class.',
                'gtw'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $mode = $input->getOption('mode');

        $questionHelper = $this->getHelper('question');
        $class = $questionHelper->ask(
            $input,
            $output,
            new Question('Class name: ')
        );
        $module = $questionHelper->ask(
            $input,
            $output,
            new Question('Module name: ')
        );
        $psr = $questionHelper->ask(
            $input,
            $output,
            new Question('psr-4: ')
        );

        $service = null;

        if ($mode === 'evt') {
            $service = $this->container->get(EVTService::class);
        }
        if ($mode === 'lst') {
            $service = $this->container->get(LSTService::class);
        }
        if ($mode === 'gtw') {
            $service = $this->container->get(GTWService::class);
        }

        if ($service === null) {
            $this->setModeErrorOutput($output, $mode);

            return 0;
        }

        $service->createClass($output, $psr, $module, $class);

        return 0;
    }

    protected function setModeErrorOutput(OutputInterface $output, string $mode): void
    {
        $output->getFormatter()->setStyle(
            'title',
            new OutputFormatterStyle('red', 'white', ['bold'])
        );

        $output->writeln([
            '',
            '<title>                                                                          </>',
            '<title>  ERROR:                                                                  </>',
            '<title>                                                                          </>',
            '',
            $this->setModeErrorMessage($mode),
            ''
        ]);
    }

    protected function setModeErrorMessage(string $mode): string
    {
        return sprintf(
            '<fg=red;options=bold> Incorrect mode value: %s. </>',
            $mode
        );
    }
}
